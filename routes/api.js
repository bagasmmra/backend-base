const express = require("express");
const {catchErrors} = require("../handlers/errorHandlers");

const router = express.Router();

const userController = require("../controllers/userController");
const absenController = require("../controllers/absenController");

router.route("/user-employee/create").post(catchErrors(userController.create));
router.route("/user-employee/update/:id").patch(catchErrors(userController.update));
router.route("/user-employee/delete/:id").delete(catchErrors(userController.delete));
router.route("/user-employee/list").get(catchErrors(userController.list));


router.route("/absent-employee/absent-in").post(catchErrors(absenController.absentIn));
router.route("/absent-employee/absent-out").post(catchErrors(absenController.absentOut));
router.route("/absent-employee/list").get(catchErrors(absenController.listAbsence));

router.route("/user/update-profile/:id").patch(catchErrors(userController.updateProfile));

module.exports = router;