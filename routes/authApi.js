const express = require("express");

const router = express.Router();

const {catchErrors} = require("../handlers/errorHandlers");
const {
    login, logout
} = require("../controllers/authController");

router.route("/login").post(catchErrors(login));
router.route("/logout").post(catchErrors(logout));

module.exports = router;