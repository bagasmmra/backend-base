const bcrypt = require("bcryptjs");
const {PrismaClient} = require("@prisma/client");
const prisma = new PrismaClient();

exports.create = async (req, res) => {
    try {
        // Extract user data from the request
        const {role, name, email, position, phone, password} = req.body;
        const salt = await bcrypt.genSalt();
        const passwordHash = await bcrypt.hash(password, salt);

        const existingAdmin = await prisma.user.findUnique({
            where: {
                email: email
            }
        });
        // console.log("Existing Admin : ", existingAdmin);
        if (existingAdmin) return res.status(400).json({msg: "An account with this email already exists."});

        if (!existingAdmin) {
            const user = await prisma.user.create({
                data: {
                    role, name, email, image: "https://res.cloudinary.com/dowikaory/image/upload/v1694244714/dookrenktyhtkv3uxsa3.jpg", position, phone, hashedPassword: passwordHash
                },
            });

            const result = {
                role: user.role,
                name: user.name,
                email: user.email,
                image: user.image,
                position: user.position,
                phone: user.phone
            }
            // Send a success response with the created user
            return res.status(200).json({
                success: true, result, message: "Successfully Created the document in Model ",
            });
        }
    } catch (error) {
        // Handle any errors that occur during the creation process
        console.error("Error creating user:", error);
        res.status(500).json({success: false, message: "Error creating user", error});
    }
};

exports.delete = async (req, res) => {
    try {
        const userId = req.params.id;
        const result = await prisma.user.delete({
            where: {
                id: userId,
            },
        });

        if (!result) {
            return res.status(404).json({
                success: false, result: null, message: "No document found by this id",
            });
        } else {
            return res.status(200).json({
                success: true, result, message: "Successfully Deleted the document by id",
            });
        }
    } catch (error) {
        // Handle any errors that occur during the creation process
        console.error("Error creating user:", error);
        return res.status(500).json({success: false, result: null, message: "Oops there is an Error",});
    }
};

exports.list = async (req, res) => {
    const page = req.query.page || 1;
    const limit = parseInt(req.query.items) || 10;
    const skip = page * limit - limit;
    let resultsPromise;
    let countPromise;

    try {
        resultsPromise = await prisma.user.findMany({
            include: {
                absen: true
            }
        });
        countPromise = prisma.user.count();
        const [result, count] = await Promise.all([resultsPromise, countPromise]);
        // Calculating total pages
        const pages = Math.ceil(count / limit);

        // Getting Pagination Object
        const pagination = {page, pages, count};

        return res.status(200).json({
            success: true, result, pagination, message: "Successfully found all documents",
        });
    } catch (error) {
        console.error('Error retrieving users:', error);
        res.status(500).json({error: 'Error retrieving users'});
    }
}

exports.update = async (req, res) => {
    try {
        const userId = req.params.id;
        const updatedUserData = req.body;

        console.log(userId)

        const updatedUser = await prisma.user.update({
            where: {id: userId}, data: updatedUserData,
        });

        // Respond with the updated user data
        if (!updatedUser) {
            return res.status(404).json({
                success: false, result: null, message: "No document found by this id",
            });
        } else {
            return res.status(200).json({
                success: true, result: updatedUser, message: "Successfully Update the document by id",
            });
        }
    } catch (error) {
        console.error('Error updating user:', error);
        return res.status(500).json({success: false, result: null, message: "Oops there is an Error",});
    }
};

exports.read = async (req, res) => {
    try {
        // Find document by id
        const result = await prisma.user.findUnique({
            where: {
                id: req.params.id
            }
        });
        // If no results found, return document not found
        if (!result) {
            return res.status(404).json({
                success: false, result: null, message: "No document found by this id: " + req.params.id,
            });
        } else {
            // Return success resposne
            return res.status(200).json({
                success: true, result, message: "we found this document by this id: " + req.params.id,
            });
        }
    } catch (err) {
        // Server Error
        return res.status(500).json({
            success: false, result: null, message: "Oops there is an Error",
        });
    }
};

exports.updateProfile = async (req, res) => {
    console.log(req.body)
    try {
        const userId = req.params.id;
        const {password, image, phone} = req.body;

        const updatedUserData = {};

        if (password) {
            const salt = await bcrypt.genSalt();
            updatedUserData.hashedPassword = await bcrypt.hash(password, salt);
        }
        if (image) {
            updatedUserData.image = image;
        }

        if (phone) {
            updatedUserData.phone = phone;
        }

        const updatedUser = await prisma.user.update({
            where: {id: userId}, data: updatedUserData,
        });

        // Respond with the updated user data
        if (!updatedUser) {
            return res.status(404).json({
                success: false, result: null, message: "No document found by this id",
            });
        } else {
            return res.status(200).json({
                success: true, result: updatedUser, message: "Successfully Update the document by id",
            });
        }
    } catch (error) {
        console.error('Error updating user:', error);
        return res.status(500).json({success: false, result: null, message: "Oops there is an Error",});
    }
};