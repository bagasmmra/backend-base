const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const {PrismaClient} = require("@prisma/client");
require("dotenv").config({path: ".env"});
const prisma = new PrismaClient();

exports.login = async (req, res) => {
    try {
        const {email, password} = req.body;

        if (email && password) {
            // Validate
            const client = await prisma.user.findFirst({where: {email}});

            if (!client) return res.status(400).json({
                success: false, result: null, message: "No account with this email has been registered.",
            });
            if (password !== client.hashedPassword){
                const isMatch = await bcrypt.compare(password, client.hashedPassword);
                if (!isMatch) return res.status(400).json({
                    success: false, result: null, message: "Invalid credentials.",
                });
            }
            const token = jwt.sign({
                exp: Math.floor(Date.now() / 1000) + 60 * 60 * 12, // 12 hours
                id: client.id,
            }, process.env.JWT_SECRET);

            const result = await prisma.user.update({
                where: {id: client.id}, data: {isLoggedIn: true},
            });

            res.json({
                success: true, result: {
                    token,
                    user: {
                        id: result.id,
                        role: result.role,
                        position: result.position,
                        name: result.name,
                        phone: result.phone,
                        email: result.email,
                        image: result.image,
                        isLoggedIn: result.isLoggedIn,
                    },
                }, message: "Successfully logged in as client",
            });
        }
    } catch (err) {
        res.status(500).json({success: false, result: null, message: err.message});
    }
};

exports.isValidToken = async (req, res, next) => {
    try {
        const token = req.header("x-auth-token");
        if (!token) return res.status(401).json({
            success: false, result: null, message: "No authentication token, authorization denied.", jwtExpired: true,
        });

        const verified = jwt.verify(token, process.env.JWT_SECRET);
        if (!verified) return res.status(401).json({
            success: false, result: null, message: "Token verification failed, authorization denied.", jwtExpired: true,
        });

        const user = await prisma.user.findUnique({_id: verified.id});
        if (!user) return res.status(401).json({
            success: false, result: null, message: "User doens't Exist, authorization denied.", jwtExpired: true,
        });

        if (user.isLoggedIn === false) return res.status(401).json({
            success: false,
            result: null,
            message: "Admin is already logout try to login, authorization denied.",
            jwtExpired: true,
        }); else {
            req.user = user;
            // console.log(req.admin);
            next();
        }
    } catch (err) {
        res.status(500).json({
            success: false, result: null, message: err.message, jwtExpired: true,
        });
    }
};

exports.logout = async (req, res) => {
    const userId = req.body.userId;
        try {
            const updatedUser = await prisma.user.update({
                where: {id: userId}, data: {isLoggedIn: false},
            });

            res.status(200).json({isLoggedIn: updatedUser.isLoggedIn});
        } catch (error) {
            console.error('Error:', error);
            res.status(500).json({error: 'An error occurred while logging out'});
        }
};