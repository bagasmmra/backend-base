const {PrismaClient} = require("@prisma/client");
const moment = require('moment-timezone');
const prisma = new PrismaClient();

const jakartaTimeZone = 'Asia/Jakarta';
exports.absentIn = async (req, res) => {
    let today = moment.tz(jakartaTimeZone);
    let formattedTime = today.format('hh:mm A');
    try {
        // Extract user data from the request
        const {userId} = req.body;
        const existingAbsentIn = await prisma.absen.findFirst({
            where: {
                userId: userId, status: "Entry"
            },
            orderBy: {
                date: 'desc'
            }
        });

        console.log(existingAbsentIn)

        if (existingAbsentIn){
            const todayStr = today.format('YYYY-MM-DD');
            const dateStr = existingAbsentIn.date.toISOString()
            const parsedDate = moment(dateStr);
            const formattedDate = parsedDate.format('YYYY-MM-DD');
            console.log(todayStr, formattedDate)
            if (todayStr <= formattedDate) {
                return res.status(400).json({
                    success: false, result: null, message: "An account with this already absent."
                });
            }else{
                const data = {
                    userId, status: "Entry", date: new Date(), time: formattedTime
                }
                const result = await prisma.absen.create({
                    data
                });
                // Send a success response with the created user
                return res.status(200).json({
                    success: true, result, message: "Successfully Created the document in Absent",
                });
            }
        }


        if (!existingAbsentIn) {
            const data = {
                userId, status: "Entry", date: new Date(), time: formattedTime
            }
            const result = await prisma.absen.create({
                data
            });
            // Send a success response with the created user
            return res.status(200).json({
                success: true, result, message: "Successfully Created the document in Absent",
            });
        }
    } catch (error) {
        // Handle any errors that occur during the creation process
        console.error("Error creating user:", error);
        res.status(500).json({success: false, message: "Error creating user", error});
    }
};

exports.absentOut = async (req, res) => {
    let clockVerif = "05:00 PM";
    let today = moment.tz(jakartaTimeZone);
    let formattedTime = today.format('hh:mm A');
    let clockVerifMoment = moment(clockVerif, 'hh:mm A');
    let formattedTimeMoment = moment(formattedTime, 'hh:mm A');

    try {
        if (clockVerifMoment.isAfter(formattedTimeMoment)) {
            return res.status(400).json({
                success: false,
                result: null,
                message: "It's not time to go home yet",
            });
        }

        const {userId} = req.body;

        const existingAbsentOut = await prisma.absen.findFirst({
            where: {
                userId: userId, status: "Out"
            },
            orderBy: {
                date: 'desc'
            }
        });

        if (existingAbsentOut){
            const todayStr = today.format('YYYY-MM-DD');
            const dateStr = existingAbsentOut.date.toISOString()
            const parsedDate = moment(dateStr);
            const formattedDate = parsedDate.format('YYYY-MM-DD');
            if (todayStr <= formattedDate) {
                return res.status(400).json({
                    success: false, result: null, message: "An account with this already absent."
                });
            }else{
                const data = {
                    userId, status: "Out", date: new Date(), time: formattedTime
                }
                const result = await prisma.absen.create({
                    data
                });
                // Send a success response with the created user
                return res.status(200).json({
                    success: true, result, message: "Successfully Created the document in Absent",
                });
            }
        }

        if (!existingAbsentOut) {
            const data = {
                userId, status: "Out", date: new Date(), time: formattedTime
            }
            const result = await prisma.absen.create({
                data
            });
            // Send a success response with the created user
            return res.status(200).json({
                success: true, result, message: "Successfully Created the document in Absent",
            });
        }
    } catch (error) {
        // Handle any errors that occur during the creation process
        console.error("Error creating user:", error);
        res.status(500).json({success: false, message: "Error creating user", error});
    }
};

exports.listAbsence = async (req, res) => {
    const absenData = await prisma.absen.findMany({
        include: {
            User: true
        },
    });
    const { month, year } = req.query; // Access the query parameters
    // const currentDate = new Date();
    // const currentMonth = currentDate.getMonth(); // September is 8 (0-indexed)
    // const currentYear = currentDate.getFullYear();
    const daysInMonth = new Date(year, month, 0).getDate();
    const headers = Array.from({ length: daysInMonth }, (_, i) => {
        const day = i + 1;
        const formattedDay = day.toString().padStart(2, '0');
        return `${formattedDay}-${month}-${year}`;
    });
    const pivotTable = {};
    absenData.forEach((entry) => {
        const { userId, status, date, time } = entry;
        const userName = entry.User.name;
        const entryDate = new Date(date);
        const entryMonth = entryDate.getMonth() + 1;
        const dayOfMonth = entryDate.getDate();
        const formattedDate = `${dayOfMonth}-${month}-${year}`;

        if (!pivotTable[userId]) {
            pivotTable[userId] = {
                userId,
                userName,
                Entry: {},
                Out: {},
            };

            headers.forEach((header) => {
                pivotTable[userId].Entry[header] = null;
                pivotTable[userId].Out[header] = null;
            });
        }

        if ((status === "Entry" || status === "Out") && (entryMonth == month)) {
            pivotTable[userId][status][formattedDate] = time;
        }
    });

    const pivotTableArray = Object.values(pivotTable);

    return res.status(200).json({
        success: true, result: pivotTableArray, message: "Successfully Created the document in Absent",
    });
}